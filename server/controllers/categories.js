const express         = require('express'),
      responseBuilder = require('../helpers').responseBuilder,
      Categories      = require('../db').models.Categories;

const categoryRouter = new express.Router();

categoryRouter
  .get('/', function(req, res) {
    Categories
      .find({}, {_id: 0})
      .sort('name')
      .select('name')
      .exec((err, categories) => {
        res.json(
          responseBuilder({
            categories: categories.reduce((list, category) => {
              list.push(category.name);
              return list;
            }, [])
          })
        );
      });
  });

module.exports = categoryRouter;
