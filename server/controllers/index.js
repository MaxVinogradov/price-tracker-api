const express = require('express');

const products   = require('./products'),
      categories = require('./categories');

const router = new express.Router();

const auth = require("./auth");

router.use('/categories', categories);
router.use('/products',   products);

router.get('/status', (req, res) => {
  res.json({ status: 'OK' });
});


// restrict index for logged in user only
router.get('/', auth.home);

// route to register page
router.get('/register', auth.register);

// route for register action
router.post('/register', auth.doRegister);

// route to login page
router.get('/login', auth.login);

// route for login action
router.post('/login', auth.doLogin);

// route for logout action
router.get('/logout', auth.logout);

module.exports = router;
