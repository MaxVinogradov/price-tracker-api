const express         = require('express'),
      responseBuilder = require('../helpers').responseBuilder,
      Products        = require('../db').models.Products;

const productRouter = new express.Router();

productRouter
  .get('/', function(req, res) {
    Products
      .find({
        category: req.query.category,
        model: new RegExp(req.query.model, 'i')
      }, {_id: 0})
      .sort('category model')
      .limit(20)
      // .select('model')
      .exec((err, products) => {
        res.json(
          responseBuilder({
            products: products.reduce((list, product) => {
              list.push(product);
              return list;
            }, [])
          })
        );
      });
  })
  .get('/:id', function(req, res) {
    Products
      .findOne({
        category: req.query.category,
        model: req.params.id
      }, {_id: 0})
      .exec((err, product) => {
        const doc = product._doc;
        const shops = doc.shops.reduce((list, shop) => {
          list.push({
            shop: shop.shop,
            url: shop.url,
            imageUrl: shop.imageUrl,
          });
          return list;
        }, []);
        res.json(responseBuilder({
          shops,
          category: doc.categorySchema,
          model: doc.model
        }));
      });
  })
;

module.exports = productRouter;
