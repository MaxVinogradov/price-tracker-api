const passport = require("passport");
const Users = require('../db').models.Users;

const userController = {};

// Restrict access to root page
userController.home = function(req, res) {
  res.json({page: 'index', user : req.user, isAuth: req.isAuthenticated() });
};

// Go to registration page
userController.register = function(req, res) {
  res.json({page: 'register', user : req.user });
};

// Post registration
userController.doRegister = function(req, res) {
  Users.register(new Users({
    username : req.body.username,
    name: req.body.name
  }), req.body.password, function(err, user) {
    if (err) {
      return res.json({ action: 'register', user : user });
    }

    passport.authenticate('local')(req, res, function () {
      res.redirect('/api/');
    });
  });
};

// Go to login page
userController.login = function(req, res) {
  res.json({ action: 'login' });
};

// Post login
userController.doLogin = function(req, res) {
  passport.authenticate('local')(req, res, function () {
    res.redirect('/api/');
  });
};

// logout
userController.logout = function(req, res) {
  req.logout();
  res.redirect('/api/');
};

module.exports = userController;
