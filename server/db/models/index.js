module.exports = {
  Users:          require('./users'),
  Products:       require('./products'),
  Subscriptions:  require('./subscriptions'),
  Categories:     require('./categories'),
};
