const mongoose = require('mongoose'),
      schemas  = require('../schemas');

const subscriptions = mongoose.model('Subscriptions', schemas.subscriptionSchema);

// Extension point for future functionality

module.exports = subscriptions;
