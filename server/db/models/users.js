const mongoose = require('mongoose'),
      schemas  = require('../schemas');

const users = mongoose.model('Users', schemas.userSchema);

// Extension point for the future functionality

module.exports = users;
