const mongoose = require('mongoose'),
      schemas  = require('../schemas');

const categories = mongoose.model('Categories', schemas.categorySchema);

// Extension point for the future functionality

module.exports = categories;
