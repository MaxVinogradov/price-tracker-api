const seeder = require('mongoose-seed'),
  configs = require('../../../configs');

const seedsData = [
  require('./productSeeds'),
  require('./userSeeds'),
  require('./subscriptionSeeds'),
  require('./categorySeeds'),
], modelPathes = [
  '../models/products',
  '../models/users',
  '../models/subscriptions',
  '../models/categories',
], modelTitles = [
  'Products',
  'Users',
  'Subscriptions',
  'Categories'
];

// TODO make database.url editable
// TODO add seeds creation as npm script
seeder.connect(configs.development.database.url, () => {
  seeder.loadModels(modelPathes);
  seeder.clearModels(modelTitles, () => {
    seeder.populateModels(seedsData, () => seeder.disconnect());
  });
});
