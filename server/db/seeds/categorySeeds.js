module.exports = {
  model: 'Categories',
  documents: [
    { name: 'smartphones' },
    { name: 'cameras' },
    { name: 'notebooks' },
  ]
};
