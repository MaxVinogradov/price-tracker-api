function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const productPricesTemplate = () => [
  { date: new Date(666666666666).toDateString(), price: getRandomInt(1000, 4000) },
  { date: new Date(777777777777).toDateString(), price: getRandomInt(1000, 3000) },
  { date: new Date(888888888888).toDateString(), price: getRandomInt(1000, 3500) },
  { date: new Date(999999999999).toDateString(), price: getRandomInt(1000, 4000) }
];

const shopTemplate = () => {
  return {
    shop: '',
    url: 'https://comfy.ua/smartfon-samsung-a530f-galaxy-a8-2018-black.html',
    imageUrl: 'https://cdn.comfy.ua/media/catalog/product/cache/4/small_image/270x265/62defc7f46f3fbfc8afcd112227d1181/d/f/dfwefwkergferfgbv-df_1.jpg',
    prices: productPricesTemplate()
}};

const updateShop = (shopTemplate, shopName) => {
  shopTemplate.shop = shopName;
  return shopTemplate;
};

module.exports = {
  model: 'Products',
  documents: [
    {
      category: 'smartphones',
      model: 'Samsung A530F Galaxy A8 2018 Black',
      shops: [
        updateShop(shopTemplate(), 'comfy.ua'),
        updateShop(shopTemplate(), 'www.foxtrot.com.ua'),
        updateShop(shopTemplate(), 'rozetka.com.ua')
      ]
    },
    {
      category: 'smartphones',
      model: 'Samsung W789 Galaxy Black',
      shops: [
        updateShop(shopTemplate(), 'comfy.ua'),
        updateShop(shopTemplate(), 'www.foxtrot.com.ua'),
        updateShop(shopTemplate(), 'rozetka.com.ua')
      ]
    },
    {
      category: 'notebooks',
      model: 'Lenovo Z510 Black',
      shops: [
        updateShop(shopTemplate(), 'comfy.ua'),
        updateShop(shopTemplate(), 'www.foxtrot.com.ua'),
        updateShop(shopTemplate(), 'rozetka.com.ua')
      ]
    },
    {
      category: 'notebooks',
      model: 'Lenovo Z710 Black',
      shops: [
        updateShop(shopTemplate(), 'comfy.ua'),
        updateShop(shopTemplate(), 'www.foxtrot.com.ua'),
        updateShop(shopTemplate(), 'rozetka.com.ua')
      ]
    }
  ]
};
