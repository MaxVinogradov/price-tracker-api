const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

module.exports = {
  mongooseConnectionInit: dbUrl => {
    mongoose.connection.on('connected', () => {
      console.log('Mongoose default connection is open to ', dbUrl);
    });

    mongoose.connection.on('error', err => {
      console.log(`Mongoose default connection has occured ${err} `);
    });

    mongoose.connection.on('disconnected', () => {
      console.log('Mongoose default connection is disconnected');
    });

    process.on('SIGINT', () => {
      mongoose.connection.close(() => {
        console.log('Mongoose default connection is disconnected due to application termination');
        process.exit(0);
      });
    });

    mongoose.connect(dbUrl);
    const db = mongoose.connection;
    db.once('open', function () {
      console.log('Connected to the database');
    });
  },
  schemas: require('./schemas'),
  models:  require('./models'),

  // seeds:   require('./seeds')
};
