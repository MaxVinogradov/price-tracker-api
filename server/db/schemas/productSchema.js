const mongoose = require('mongoose');

module.exports = mongoose.Schema({
  category: String,
  model: String,
  shops: [
    {
      shop: String,
      url: String,
      imageUrl: String,
      prices: [{date: Date, price: Number}] // stuff for subscription flow...
    }
  ]
});
