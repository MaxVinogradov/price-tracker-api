module.exports = {
  productSchema:      require('./productSchema'),
  userSchema:         require('./userSchema'),
  subscriptionSchema: require('./subscriptionSchema'),
  categorySchema:     require('./categorySchema'),
};
