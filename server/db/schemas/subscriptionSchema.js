const mongoose = require('mongoose');

module.exports = mongoose.Schema({
  user: mongoose.Schema.Types.ObjectId,
  product: mongoose.Schema.Types.ObjectId,
  notificationPeriod: Number
});
