const path = require('path');
const configs = require('./helpers/mapper-configs');

module.exports = configs(
  path.resolve(__dirname, '../configs'),
  'development',
  'current'
);
