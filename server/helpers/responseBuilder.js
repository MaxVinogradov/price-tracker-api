module.exports =  (payload, status = 'success') => {
  return {
    response: {
      status,
      ...payload
    }
  }
};
